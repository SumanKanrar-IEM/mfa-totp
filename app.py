from flask import Flask, render_template, redirect, url_for, flash, session, abort, request, escape, jsonify
from flask_bootstrap import Bootstrap
import uuid
import os
import json
import pyotp

app = Flask(__name__)
app.secret_key = 'mysecret'


bootstrap = Bootstrap(app)


@app.route('/')
def index():
    if 'username' in session:
        username = session['username']
        print(session)
        return 'Logged in as ' + username + '<br> <br>' + "<b><a href = '/logout'>click here to log out</a></b>"

    return "You are not logged in <br><br><a href = '/login'></b>" + "click here to log in</b></a>"


@app.route('/welcome')
def welcome():
    user = request.args.get("user")
    return render_template("welcome.html", user=user)


# Route for validating the Time based OTP
@app.route('/otp_validation', methods=['GET', 'POST'])
def otp_validation():
    error = None
    print("Inside OTP validation")
    totp = request.args.get("totp")
    username = request.args.get("username")
    user_key = request.args.get("user_key")
    print(totp, username, user_key)
    if request.method == 'POST':

        p = 0
        try:
            p = int(totp)
        except Exception as e:
            print(e)
            return 'Unable to read OTP'
        t = pyotp.TOTP(user_key)

        if t.verify(p):
            flash('Authentication successful!', 'success')
            return redirect(url_for('welcome', user=username))
        else:
            flash('Invalid one-time password!', 'danger')
            error = 'Invalid one-time password!'
            return redirect(url_for('otp_validation', error=error, totp=totp, username=username, user_key=user_key))

    # redirect('/otp_validation')
    return render_template('two-factor-setup.html', error=error, totp=totp, username=username, user_key=user_key)


# Route for handling the login page logic of username/password
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        # Hardcoded username and password with admin/admin for the time being.
        # Will accept username/password from fetch and do checking from database accordingly.
        if request.form['username'] != 'admin' or request.form['password'] != 'admin':
            error = 'Invalid Credentials. Please try again.'
        else:
            print("Correct Credentials")
            username = request.form['username']
            print(username)
            session = create_session(username)
            print("Printing session in login function: ", session)

            print("Printing json.loads in login function: ", json.loads(session))
            session = json.loads(session)

            user_key = session['user_key']
            session_id = session['session_id']
            print(user_key, session_id)

            # Generate the OTP here
            totp = generate_otp(user_key)

            # Send otp to user via mail
            send_otp_to_user()

            print("Successfully passed first step verification")
            # redirect('/otp_validation')
            # return render_template('two-factor-setup.html', totp=totp, username=session['username'])
            return redirect(url_for('otp_validation', totp=totp, username=session['username'], user_key=session['user_key'], _method='GET', _external=True))
            # return redirect(url_for('index'))

    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    # remove the username from the session if it is there
    session.pop('username', None)
    session.pop('uid', None)
    session['loggedIn'] = False
    session.pop('user_key', None)
    print("session deleted", session)
    return redirect(url_for('index'))


def create_session(username):
    session = {}
    session['username'] = username
    session['session_id'] = str(uuid.uuid4())
    print(session['session_id'])
    session['loggedIn'] = True
    session['user_key'] = pyotp.random_base32()

    print("Printing session in create_session function")
    print(session)
    print("JSON.DUMPS: ", json.dumps(session))
    return json.dumps(session)

    # bot_server_response = json.loads(bot_server_response)
    # return bot_server_response


def generate_otp(user_key):
    print("Inside generate_otp: ")
    totp_key = user_key
    if totp_key is None:
        return ''
    t = pyotp.TOTP(totp_key)
    print("Generated OTP: ", str(t.now()))
    return str(t.now())


def send_otp_to_user():
    print("OTP sent to user")
    print("Later we will implement SMS/mail or using Google Authenticator")


if __name__ == '__main__':
    app_host = os.environ.get('APP_HOST') or '0.0.0.0'
    app_port = os.environ.get('APP_PORT') or '8000'
    app.run(debug=True, host=str(app_host), port=int(app_port))
    # app.run(debug=True)
